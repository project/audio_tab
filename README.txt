**************************************************************
       'NODE AUDIO GALLERY'  MODULE FOR DRUPAL 4.7.x
**************************************************************
Current Maintainer : Suuch (http://drupal.org/user/23178)
Project Page: http://drupal.org/project/audio_tab
Support: http://sbs.suuch.com/drupal/modules/audio_tab

Dependencies
Requires Drupal 4.7.x
Requires Audio module for Drupal 4.7.x (http://drupal.org/project/audio)
Requires Flash capability in viewer's browser.

Related Modules
Image Tab module for Drupal 4.7.x (http://drupal.org/project/image_tab)
